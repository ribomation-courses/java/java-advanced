package ribomation.java_advanced.pipeline;

import ribomation.java_advanced.pipeline.threads.Consumer;

public class SingleConsumerApp {
    public static void main(String[] args) throws InterruptedException {
        Consumer consumer = new Consumer();
        consumer.start();
        for (long k = 1; k <= 10; ++k) {
            consumer.send(k);
        }
        consumer.send(0L);
        consumer.join();
    }
}
