package ribomation.java_advanced.pipeline;


import ribomation.java_advanced.pipeline.threads.Consumer;
import ribomation.java_advanced.pipeline.threads.Producer;
import ribomation.java_advanced.pipeline.threads.Transformer;

public class ProdTransConsApp {
    public static void main(String[] args) throws Exception {
        new ProdTransConsApp().run(50);
    }

    void run(int n) throws InterruptedException {
        Consumer    c = new Consumer();
        Transformer t = new Transformer(c);
        Producer    p = new Producer(n, t);
        c.start();
        t.start();
        p.start();
        c.join();
    }
}
