package ribomation.java_advanced.pipeline;


import ribomation.java_advanced.pipeline.threads.Consumer;
import ribomation.java_advanced.pipeline.threads.Producer;

public class ProdConsApp {
    public static void main(String[] args) throws Exception {
        new ProdConsApp().run(100);
    }
    void run(int n) throws InterruptedException {
        Consumer c = new Consumer();
        Producer p = new Producer(n, c);
        c.start();
        p.start();
        c.join();
    }
}
