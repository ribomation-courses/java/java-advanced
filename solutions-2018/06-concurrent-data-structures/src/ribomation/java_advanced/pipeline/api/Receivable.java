package ribomation.java_advanced.pipeline.api;

public interface Receivable<MessageType> {
    void send(MessageType msg);
}
