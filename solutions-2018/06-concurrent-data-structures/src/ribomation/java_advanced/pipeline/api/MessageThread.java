package ribomation.java_advanced.pipeline.api;

import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;

public abstract class MessageThread<MessageType>
        extends Thread
        implements Receivable<MessageType>
{
    private BlockingQueue<MessageType> inbox;

    protected MessageThread(int maxMessages) {
        inbox = new ArrayBlockingQueue<>(maxMessages);
    }

    protected MessageThread() {
        this(10);
    }

    @Override
    public void send(MessageType msg) {
        try {
            inbox.put(msg);
        } catch (InterruptedException ignore) { }
    }

    protected MessageType recv() {
        try {
            return inbox.take();
        } catch (InterruptedException ignore) { }
        return null;
    }
}
