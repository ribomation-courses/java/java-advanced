package ribomation.java_advanced.pipeline.threads;

import ribomation.java_advanced.pipeline.api.MessageThread;
import ribomation.java_advanced.pipeline.api.Receivable;

public class Transformer extends MessageThread<Long> {
    private final Receivable<Long> next;

    public Transformer(Receivable<Long> next) {
        this.next = next;
    }

    @Override
    public void run() {
        long msg;
        while ((msg = recv()) > 0) {
            next.send(2 * msg);
        }
        next.send(-1L);
    }
}
