package ribomation.java_advanced.pipeline.threads;

import ribomation.java_advanced.pipeline.api.MessageThread;

public class Consumer extends MessageThread<Long> {
    @Override
    public void run() {
        long msg;
        while ((msg = recv()) > 0) {
            System.out.printf("[consumer] %d%n", msg);
        }
    }
}
