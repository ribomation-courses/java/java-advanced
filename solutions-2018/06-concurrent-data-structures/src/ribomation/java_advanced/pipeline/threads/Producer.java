package ribomation.java_advanced.pipeline.threads;

import ribomation.java_advanced.pipeline.api.Receivable;

public class Producer extends Thread {
    private final int              maxNumber;
    private final Receivable<Long> next;

    public Producer(int maxNumber, Receivable<Long> next) {
        this.maxNumber = maxNumber;
        this.next = next;
    }

    @Override
    public void run() {
        for (long k = 1; k <= maxNumber; ++k) next.send(k);
        next.send(-1L);
    }
}
