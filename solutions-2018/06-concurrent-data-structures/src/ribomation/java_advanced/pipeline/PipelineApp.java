package ribomation.java_advanced.pipeline;

import ribomation.java_advanced.pipeline.api.Receivable;
import ribomation.java_advanced.pipeline.threads.Consumer;
import ribomation.java_advanced.pipeline.threads.Producer;
import ribomation.java_advanced.pipeline.threads.Transformer;

public class PipelineApp {
    public static void main(String[] args) throws Exception {
        new PipelineApp().run(args);
    }

    void run(String[] args) throws Exception {
        int numTransformer = 10;
        int maxNumber      = 10;

        for (String arg : args) {
            if (arg.startsWith("--trans=")) {
                numTransformer = Integer.parseInt(arg.substring(8));
            } else if (arg.startsWith("--max=")) {
                maxNumber = Integer.parseInt(arg.substring(6));
            }
        }

        System.out.printf("# transformers: %d%n", numTransformer);
        System.out.printf("Max number    : %d%n", maxNumber);

        run(numTransformer,maxNumber);
    }

    void run(int numTransformer, int maxNumber) throws InterruptedException {
        Consumer c = new Consumer();
        c.start();

        Receivable<Long> next = c;
        while (numTransformer-- > 0) {
            Transformer t = new Transformer(next);
            t.start();
            next = t;
        }

        Producer p = new Producer(maxNumber, next);
        p.start();

        c.join();
    }
}
