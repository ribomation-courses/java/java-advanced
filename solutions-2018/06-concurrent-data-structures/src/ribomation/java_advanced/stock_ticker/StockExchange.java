package ribomation.java_advanced.stock_ticker;

import java.util.ArrayList;
import java.util.List;

public class StockExchange {
    public static void main(String[] args) throws Exception {
        new StockExchange().run(5);
    }

    StockTicker  ticker;
    Updater      updater;
    List<Reader> readers;

    void run(int numReaders) throws Exception {
        setup(numReaders);
        launch();
        stop(10);
    }

    void setup(int numReaders) {
        ticker = new StockTicker("GOOG");
        updater = new Updater(ticker);
        readers = new ArrayList<>();
        for (int k = 0; k < numReaders; ++k) {
            readers.add(new Reader(k + 1, ticker));
        }
    }

    void launch() {
        updater.start();
        for (Reader reader : readers) {
            reader.start();
        }
    }

    void stop(int numSeconds) throws InterruptedException {
        Thread.sleep(numSeconds*1000);
        updater.interrupt();
        for (Reader reader : readers) {
            reader.interrupt();
        }
    }

}
