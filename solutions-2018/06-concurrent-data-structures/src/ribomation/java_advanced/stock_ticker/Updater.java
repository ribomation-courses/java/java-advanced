package ribomation.java_advanced.stock_ticker;


import java.util.Random;

public class Updater extends Thread {
    private final StockTicker ticker;

    public Updater(StockTicker ticker) {
        this.ticker = ticker;
    }

    @Override
    public void run() {
        Random r = new Random(System.nanoTime() % 97);
        try {
            do {
                float              amount = r.nextFloat() * 10 - 3F;
                int                count  = r.nextInt(1000);
                StockTicker.Result result = ticker.update(amount, count);
                System.out.printf("*** [updater] ticker: %s *****************%n", result);
                sleep(1000);
            } while (!interrupted());
        } catch (InterruptedException ignore) {
        }
        System.out.println("*** [updater] TERMINATED");
    }
}
