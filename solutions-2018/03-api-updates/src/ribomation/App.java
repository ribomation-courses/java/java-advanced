package ribomation;

public class App {
    public static void main(String[] args) {
        new App().run(5);
    }

    void run(int N) {
        OptList<String> list = mk(N);

        System.out.printf("list[%d] = %s%n", N - 2,
                list.lookup(N - 2).orElse("nope"));

        System.out.printf("list[%d] = %s%n", N + 2,
                list.lookup(N + 2).orElse("nope"));
    }

    OptList<String> mk(int n) {
        OptList<String> lst = new OptList<>();

        for (int k = 0; k < n; ++k)
            lst.add(String.format("str-%d", k));

        return lst;
    }
}
