package ribomation.java_advanced;

public class TransformingText {
    static void invoke(String name, TextTransform f) {
        String txt    = "Hello Java";
        String result = f.transform(txt);
        System.out.printf("%-10s: %s -> %s%n", name, txt, result);
    }

    public static void main(String[] args) {
        invoke("Anon Class", new TextTransform() {
            @Override
            public String transform(String txt) {
                return txt.toUpperCase();
            }
        });

        invoke("Lambda", s -> s.toUpperCase());

        invoke("Method Ref", String::toUpperCase);
    }
}
