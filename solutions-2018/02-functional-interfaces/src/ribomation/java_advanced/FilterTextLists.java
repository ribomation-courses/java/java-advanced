package ribomation.java_advanced;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.function.Predicate;

public class FilterTextLists {
    static List<String> filter(List<String> lst, Predicate<String> p) {
        List<String> result = new ArrayList<>();
        for (String s : lst) if (p.negate().test(s)) result.add(s);
        return result;
    }

    static void invoke(String name, Predicate<String> p) {
        List<String> words  = Arrays.asList("", "Java", "", "is", "", "a", "", "cool", "", "language", "");
        List<String> result = filter(words, p);
        System.out.printf("%-10s: %s -> %s%n", name, words, result);
    }

    public static void main(String[] args) {
        invoke("Anon Class", new Predicate<String>() {
            @Override
            public boolean test(String s) {
                return s.isEmpty();
            }
        });

        invoke("Lambda", s -> s.isEmpty());

        invoke("Method Ref", String::isEmpty);
    }
}
