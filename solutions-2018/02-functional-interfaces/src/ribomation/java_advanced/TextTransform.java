package ribomation.java_advanced;

interface TextTransform {
    String transform(String txt);
}
