package ribomation.java_advanced;

public class Account {
    private final int accno;
    private int balance;

    public Account(int accno, int balance) {
        this.accno = accno;
        this.balance = balance;
    }

    public static void transfer(int agentId, Account from, Account to, int amount) {
        synchronized (from) {
            System.out.printf("[agent-%d] LOCKED %d%n", agentId, from.getAccno());
            synchronized (to) {
                from.balance -= amount;
                to.balance   += amount;
                System.out.printf("[agent-%d] SEK %d: %d [%d] --> %d [%d]%n",
                        agentId, amount,
                        from.getAccno(), from.getBalance(),
                        to.getAccno(), to.getBalance());
            }
        }
    }

    @Override
    public String toString() {
        return "Account{accno=" + accno + ", balance=" + balance + '}';
    }

    public int getAccno() {
        return accno;
    }

    public int getBalance() {
        return balance;
    }

}
