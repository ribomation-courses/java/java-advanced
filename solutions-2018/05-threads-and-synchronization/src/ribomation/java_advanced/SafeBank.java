package ribomation.java_advanced;

import java.util.List;

public class SafeBank extends Bank {
    public static void main(String[] args) throws Exception {
        new SafeBank().run();
    }

    @Override
    TransferAgent createTransferAgent(int id, int transfers, int amt, List<Account> accounts) {
        return new TransferAgent(id, transfers, amt, accounts) {
            @Override
            protected Account[] pickTwoAccount(List<Account> accounts) {
                Account[] twoAccounts = super.pickTwoAccount(accounts);

                //define an allocation order: lock the account with the lowest accno first
                if (twoAccounts[0].getAccno() < twoAccounts[1].getAccno()) {
                    return twoAccounts;
                } else {
                    return new Account[]{twoAccounts[1], twoAccounts[0]};
                }
            }
        };
    }
}
