package ribomation.java_advanced;

import java.util.List;
import java.util.concurrent.ThreadLocalRandom;

public class TransferAgent extends Thread {
    private final        int               id;
    private              int               numTransfers;
    private final        int               amount;
    private              List<Account>     accounts;

    public TransferAgent(int id, int numTransfers, int amount, List<Account> accounts) {
        super("TransferAgent-" + id);
        this.id = id;
        this.numTransfers = numTransfers;
        this.amount = amount;
        this.accounts = accounts;
    }

    @Override
    public void run() {
        while (numTransfers-- > 0) {
            Account[] acc  = pickTwoAccount(this.accounts);
            Account   from = acc[0];
            Account   to   = acc[1];

            Account.transfer(id, from, to, amount);
        }
    }

    protected Account[] pickTwoAccount(List<Account> accounts) {
        Account a = accounts.get(nextAccountIndex(accounts));
        Account b;
        do {
            b = accounts.get(nextAccountIndex(accounts));
        } while (a.getAccno() == b.getAccno());

        return new Account[]{a, b};
    }

    private static int nextAccountIndex(List<Account> accounts) {
        return ThreadLocalRandom.current().nextInt(accounts.size());
    }

}


