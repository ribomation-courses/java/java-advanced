package ribomation.java_advanced;

import java.util.ArrayList;
import java.util.List;

public class Bank {
    public static void main(String[] args) throws Exception {
        new Bank().run();
    }

    List<Account> accounts;
    List<TransferAgent> agents;

    void run() throws Exception {
        setup(8, 0, 10, 10_000);

        int totalBefore = sumBalances(accounts);
        System.out.printf("[before] accounts: %s%n", accounts);
        System.out.printf("[before] SUM=%d%n", totalBefore);

        launch(agents);

        System.out.println("--------");
        System.out.printf("[after] accounts: %s%n", accounts);
        System.out.printf("[before] total balance = %d%n", totalBefore);
        System.out.printf("[after ] total balance = %d%n", sumBalances(accounts));
    }

    void setup(int numAccounts, int initialBalance, int numAgents, int numTransfers) {
        accounts = new ArrayList<>();
        for (int k = 0; k < numAccounts; ++k) {
            accounts.add(new Account(k + 1, initialBalance));
        }

        agents = new ArrayList<>();
        for (int k = 0; k < numAgents; ++k) {
            agents.add(createTransferAgent(k + 1, numTransfers, 100, accounts));
        }
    }

    TransferAgent createTransferAgent(int id, int transfers, int amt, List<Account> accounts) {
        return new TransferAgent(id, transfers, amt, accounts);
    }

    void launch(List<TransferAgent> agents) throws InterruptedException {
        for (TransferAgent agent : agents) agent.start();
        for (TransferAgent agent : agents) agent.join();
    }

    int sumBalances(List<Account> accounts) {
        return accounts.stream()
                .mapToInt(Account::getBalance)
                .sum();
    }

}
