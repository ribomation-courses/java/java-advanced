package ribomation.java_advanced;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.List;

public class FileStats {
    public static void main(String[] args) throws Exception {
        new FileStats().fileStats("../../../", 10);
    }

    void fileStats(String dir, int maxResults) throws Exception {
        List<Process> pipeline = ProcessBuilder.startPipeline(List.of(
                new ProcessBuilder("find", dir, "-type", "f", "-name", "*.java"),
                new ProcessBuilder("xargs", "wc"),
                new ProcessBuilder("grep", "-v", "total"),
                new ProcessBuilder("sort", "-nr", "-k", "2"),
                new ProcessBuilder("head", "-" + maxResults)
        ));

        Process last = pipeline.get(pipeline.size() - 1);
        if (last.isAlive()) {
            BufferedReader in = new BufferedReader(
                    new InputStreamReader(
                            last.getInputStream()));
            try (in) {
                in.lines().forEach(System.out::println);
            }
        }
    }
}
