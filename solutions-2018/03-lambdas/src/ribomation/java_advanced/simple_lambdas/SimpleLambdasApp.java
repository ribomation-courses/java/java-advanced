package ribomation.java_advanced.simple_lambdas;

import java.util.Arrays;
import java.util.List;

import static ribomation.java_advanced.simple_lambdas.SimpleLambdas.*;

public class SimpleLambdasApp {
    public static void main(String[] args) {
        SimpleLambdasApp app = new SimpleLambdasApp();
        app.using_repeat();
        app.using_apply();
    }

    void using_repeat() {
        System.out.print("Repeat: ");
        repeat(10, (k) -> System.out.printf("%s-%d ", "Java", k));
        System.out.println();
    }

    void using_apply() {
        System.out.print("Apply : ");
        List<Integer> integers = Arrays.asList(1, 2, 3, 4, 5, 6, 7, 8, 9, 10);
        System.out.println(apply(integers, (n) -> n * n));
        System.out.println(apply(integers, (x) -> x + 42));
    }
}
