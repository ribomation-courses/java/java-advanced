package ribomation.java_advanced.simple_lambdas;

public interface Apply<T> {
    T transform(T x);
}
