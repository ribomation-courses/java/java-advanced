package ribomation.java_advanced.simple_lambdas;

import java.util.ArrayList;
import java.util.List;

public class SimpleLambdas {

    public static void repeat(int count, Repeat expr) {
        for (int k = 1; k <= count; ++k) expr.doit(k);
    }

    public static <T> List<T> apply(List<T> list, Apply<T> f) {
        List<T> result = new ArrayList<>();
        for (T item : list) result.add(f.transform(item));
        return result;
    }

}
