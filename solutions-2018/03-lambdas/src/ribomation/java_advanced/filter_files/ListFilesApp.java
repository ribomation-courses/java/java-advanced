package ribomation.java_advanced.filter_files;
import java.io.File;
import java.io.IOException;
import java.util.Arrays;

public class ListFilesApp {
    public static void main(String[] args) throws Exception {
        ListFilesApp app    = new ListFilesApp();
        String       pkgDir = ListFilesApp.class.getPackage().getName().replace('.', '/');
        app.run(new File("./src/" + pkgDir));
    }

    void run(File dir) throws IOException {
        System.out.printf("DIR: %s%n", dir.getCanonicalPath());
        if (!dir.isDirectory()) {
            throw new RuntimeException("not a directory: " + dir);
        }

        File[] files = dir.listFiles(f ->
                f.isFile()
             && f.getName().endsWith(".java")
             && f.length() > 1000
        );
        assert files != null;

        Arrays.asList(files).forEach(System.out::println);
                                 // f -> System.out.println(f)
    }
}
