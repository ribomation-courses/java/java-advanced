package ribomation.java_advanced.filter_files;

import java.util.Arrays;

public class AnotherFakeApp {
    public static void main(String[] args) {
        AnotherFakeApp app = new AnotherFakeApp();
        app.run();
    }

     void run() {
         String[] words = create();
         System.out.printf("Initial: %s%n", Arrays.toString(words));

         words = sortLexicalAsc(words);
         System.out.printf("Lexical ASC: %s%n", Arrays.toString(words));

         words = sortLexicalDesc(words);
         System.out.printf("Lexical DSC: %s%n", Arrays.toString(words));

         words = sortSizeDesc(words);
         System.out.printf("Size   : %s%n", Arrays.toString(words));
     }

    String[] create() {
        return new String[]{
                "Java", "is", "a", "cool", "language", "!!!"
        };
    }

    String[] sortLexicalAsc(String[] words) {
        Arrays.sort(words, (left, right) -> left.compareTo(right));
        return words;
    }

    String[] sortLexicalDesc(String[] words) {
        Arrays.sort(words, (left, right) -> right.compareTo(left));
        return words;
    }

    String[] sortSizeDesc(String[] words) {
        Arrays.sort(words, (left, right) -> Integer.compare(right.length(), left.length()));
        return words;
    }

}
