package ribomation.java_advanced;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.concurrent.CompletableFuture;
import java.util.stream.Collectors;

public class CourseList {
    public static void main(String[] args) throws Exception {
        String url         = "https://www.ribomation.se/";
        String cssSelector = "#courses-list ul li a.course";
        new CourseList().run(url, cssSelector);
    }

    private void run(String url, String css) throws MalformedURLException {
        fetchDoc(new URL(url))
                .thenApply(doc -> doc.select(css))
                .thenApply(spanTags ->
                        spanTags.stream()
                                .filter(link -> !link.attr("href").endsWith("index.html"))
                                .map(Element::text)
                                .sorted()
                                .collect(Collectors.toList()))
                .thenAccept(courseList -> {
                    final int[] lineno = {1};
                    courseList.forEach(course ->
                            System.out.printf("%s) %s%n", lineno[0]++, course));
                })
                .join();
    }

    private CompletableFuture<Document> fetchDoc(URL url) {
        return CompletableFuture.supplyAsync(() -> {
            try {
                return Jsoup.connect(url.toString()).get();
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
        });
    }
}
