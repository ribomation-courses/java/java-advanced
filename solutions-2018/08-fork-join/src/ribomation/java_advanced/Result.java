package ribomation.java_advanced;

public class Result {
    long numFiles = 0;
    long numBytes = 0;

    public Result() {
    }

    public Result(long numFiles, long numBytes) {
        this.numFiles = numFiles;
        this.numBytes = numBytes;
    }

    public Result add(long length) {
        this.numFiles++;
        this.numBytes += length;
        return this;
    }

    public Result add(Result r) {
        this.numFiles += r.numFiles;
        this.numBytes += r.numBytes;
        return this;
    }
}
