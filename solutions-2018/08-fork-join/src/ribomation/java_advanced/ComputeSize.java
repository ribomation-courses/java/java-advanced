package ribomation.java_advanced;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ForkJoinTask;
import java.util.concurrent.RecursiveTask;

public class ComputeSize extends RecursiveTask<Result> {
    private File dir;

    public ComputeSize(File dir) {
        this.dir = dir;
    }

    @Override
    protected Result compute() {
        Result result = new Result();

        List<ComputeSize> tasks = new ArrayList<>();
        for (File f : dir.listFiles()) {
            if (f.isFile()) {
                result.add(f.length());
            } else if (f.isDirectory()) {
                ComputeSize task = new ComputeSize(f);
                task.fork();
                tasks.add(task);
            }
        }

        return tasks.stream()
                .map(ForkJoinTask::join)
                .reduce(result, Result::add)
                ;
    }

}
