package ribomation.java_advanced;

import java.io.File;
import java.io.IOException;
import java.util.Locale;
import java.util.concurrent.ForkJoinPool;
import java.util.concurrent.ForkJoinTask;

public class DirSizeApp {
    public static void main(String[] args) throws IOException {
        new DirSizeApp().run(new File("../../../../../"));
    }

    void run(File dir) throws IOException {
        System.out.printf("Scanning dir %s...%n", dir);

        long   start  = System.nanoTime();
        Result result = compute(create(dir, false));
        long   end    = System.nanoTime();

        System.out.printf(Locale.ENGLISH,
                "DIR: %s has%n %,d files of a total of %,.3f GB (elapsed %.3f secs)%n",
                dir.getCanonicalPath(),
                result.numFiles,
                result.numBytes / (1024 * 1024 * 1024.0),
                (end - start) * 1E-9
        );
    }

    Result compute(ForkJoinTask<Result> rootTask) {
        return new ForkJoinPool().invoke(rootTask);
    }

    ForkJoinTask<Result> create(File dir, boolean fancy) {
        if (fancy) return new ComputeSize2(dir);
        return new ComputeSize(dir);
    }

}
