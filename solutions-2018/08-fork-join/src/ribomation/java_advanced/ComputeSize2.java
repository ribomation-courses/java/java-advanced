package ribomation.java_advanced;

import java.io.File;
import java.util.concurrent.ForkJoinTask;
import java.util.concurrent.RecursiveTask;
import java.util.stream.Stream;

public class ComputeSize2 extends RecursiveTask<Result> {
    private File dir;
    public ComputeSize2(File dir) { this.dir = dir; }

    @Override
    protected Result compute() {
        Result result = new Result();
        return Stream.of(dir.listFiles())
                .peek(f -> {
                    if (f.isFile()) result.add(f.length());
                })
                .filter(File::isDirectory)
                .map(d -> new ComputeSize2(d).fork())
                .map(ForkJoinTask::join)
                .reduce(result, Result::add)
                ;
    }
}
