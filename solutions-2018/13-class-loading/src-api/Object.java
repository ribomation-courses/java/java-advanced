package java.lang;

public class Object {
    private static native void registerNatives();
    
    static {
        registerNatives();
    }
    
    public       native int hashCode();
    protected    native Object clone() throws CloneNotSupportedException;
    public final native Class<?> getClass();
    public final native void notify();
    public final native void notifyAll();
    public final native void wait(long timeout) throws InterruptedException;
    
    public Object() {
        //System.err.println("*** new Object @ " + hashCode());        
    }
    protected void finalize() throws Throwable { 
        //System.err.println("*** finalize @ " + hashCode());
    }

    public boolean equals(Object obj) {
        System.out.println("*** equals @ " + hashCode());
        return (this == obj);
    }
    
    public String toString() {
        System.out.println("HELLO FROM java.lang.Object !!!");
        System.out.println("*** toString @ " + hashCode());
        return getClass().getName() + "@" + Integer.toHexString(hashCode());
    }

    
    
    public final void wait() throws InterruptedException {
        wait(0);
    }
    
    public final void wait(long timeout, int nanos) throws InterruptedException {
        if (timeout < 0) {
            throw new IllegalArgumentException("timeout value is negative");
        }

        if (nanos < 0 || nanos > 999999) {
            throw new IllegalArgumentException(
                                "nanosecond timeout value out of range");
        }

        if (nanos >= 500000 || (nanos != 0 && timeout == 0)) {
            timeout++;
        }

        wait(timeout);
    }
        
}
