#!/usr/bin/env bash
set -e
set -x

APP_MAIN=App
APP_CLS_DIR=./classes-app
API_JAR=api.jar

echo "--- Running without modified API ---"
java -cp $APP_CLS_DIR $APP_MAIN

echo "--- Running with modified API ---"
java -Xbootclasspath/p:$API_JAR -cp $APP_CLS_DIR $APP_MAIN

