#!/usr/bin/env bash
set -e
set -x

APP_SRC_DIR=./src-app
APP_CLS_DIR=./classes-app

API_SRC_DIR=./src-api
API_CLS_DIR=./classes-api
API_JAR=api.jar

rm -rf $APP_CLS_DIR
rm -rf $API_CLS_DIR
mkdir -p $APP_CLS_DIR
mkdir -p $API_CLS_DIR

javac -source 1.8 -target 1.8 -cp $APP_CLS_DIR -d $APP_CLS_DIR $APP_SRC_DIR/*.java
javac -source 1.8 -target 1.8 -cp $API_CLS_DIR -d $API_CLS_DIR $API_SRC_DIR/*.java

jar cvf $API_JAR -C $API_CLS_DIR .
jar tvf $API_JAR

tree

