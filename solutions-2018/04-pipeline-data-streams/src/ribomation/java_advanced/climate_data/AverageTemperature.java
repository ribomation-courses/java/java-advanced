package ribomation.java_advanced.climate_data;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.AbstractMap;
import java.util.Map;
import java.util.TreeMap;
import java.util.function.Supplier;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import java.util.zip.GZIPInputStream;

import static java.util.stream.Collectors.averagingDouble;
import static java.util.stream.Collectors.groupingBy;

public class AverageTemperature {
    static  String                         myFile   = "C:\\Users\\jensr\\Dropbox\\Ribomation\\Ribomation-Training-2017-Autumn\\large-files\\climate-data.txt.gz";
    static  String                         webFile  = "https://docs.ribomation.se/java/java-8/climate-data.txt.gz";
    static  boolean                        parallel = true;
    private Supplier<Map<Integer, Double>> stmts;

    public static void main(String[] args) throws Exception {
        // read from file at disk
        new AverageTemperature().run(Paths.get(myFile));

        // read straight from the web
        //new AverageTemperature().run(new URL(webFile).openStream());
    }

    void run(Path path) throws IOException {
        if (!Files.isReadable(path)) {
            throw new RuntimeException("cannot read file: " + path);
        }
        run(Files.newInputStream(path));
    }

    void run(InputStream is) throws IOException {
        Map<Integer, Double> result = elapsed("Climate Data", () ->
                run(createStreamSource(is, parallel))
        );

        new TreeMap<>(result).forEach((year, temp) ->
                System.out.printf("%d: %.2f C%n", year, temp)
        );
    }

    Stream<String> createStreamSource(InputStream is, boolean parallel) {
        try {
            BufferedReader reader = new BufferedReader(new InputStreamReader(new GZIPInputStream(is)));
            Stream<String> lines  = reader.lines();
            return parallel ? lines.parallel() : lines;
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    Map<Integer, Double> run(Stream<String> lines) {
        return lines
                .filter(line -> !line.startsWith("STN--"))
                .map(line -> {
                    String[] fields = line.split("\\s+");
                    return new String[]{fields[2], fields[3]};
                })
                .map(dateAndTemperature -> {
                    LocalDate date       = LocalDate.parse(dateAndTemperature[0], DateTimeFormatter.BASIC_ISO_DATE);
                    double    fahrenheit = Double.parseDouble(dateAndTemperature[1]);
                    return new AbstractMap.SimpleEntry<>(date, toCelsius(fahrenheit));
                })
                .collect(groupingBy(dateAndTemperature ->
                                dateAndTemperature.getKey().getYear(),
                        averagingDouble(Map.Entry::getValue)));
    }

    double toCelsius(double fahrenheit) {
        return (5D / 9D) * (fahrenheit - 32D);
    }

    <T> T elapsed(String name, Supplier<T> stmts) {
        System.out.printf("[%s] running...%n", name);
        long start = System.nanoTime();
        try {
            return stmts.get();
        } finally {
            long end = System.nanoTime();
            System.out.printf("[%s] elapsed %.3f seconds%n",
                    name, (end - start) * 1E-9);
        }
    }

}
