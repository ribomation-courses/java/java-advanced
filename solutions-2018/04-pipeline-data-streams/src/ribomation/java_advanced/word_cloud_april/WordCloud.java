package ribomation.java_advanced.word_cloud_april;

import java.io.BufferedWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.function.Supplier;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import static java.util.stream.Collectors.groupingBy;
import static java.util.stream.Collectors.counting;

public class WordCloud {

    public static void main(String[] args) throws IOException {
        WordCloud app = new WordCloud();
//        app.run("./data/musketeers.txt", 4, 100);
        app.run("./data/shakespeare.txt", 5, 100);
    }

    private void run(String filename, int minWordSize, int maxWords) throws IOException {
        final Pattern whiteSpace = Pattern.compile("[^a-zA-Z]+");

        Path inPath = Paths.get(filename);
        List<Map.Entry<String, Long>> wordFreqs = Files
                .lines(inPath)
                .flatMap(whiteSpace::splitAsStream)
                .filter(word -> word.length() >= minWordSize)
                .collect(groupingBy(String::toLowerCase, counting()))
                .entrySet().stream()
                .sorted((left, right) -> right.getValue().compareTo(left.getValue()))
                .limit(maxWords)
                .collect(Collectors.toList());

        String name = basename(inPath);
        String htmlStart = String.format("<html><head>" +
                "<style> " +
                "  body {font-size: 16pt;} " +
                "  h1 {text-align:center; font-size:200%%; color:green;} " +
                "</style>" +
                "</head><body>" +
                "<h1>%s Word Cloud</h1>", name);
        String htmlEnd     = "</body></html>";
        long   maxFreq     = wordFreqs.get(0).getValue();
        long   minFreq     = wordFreqs.get(wordFreqs.size() - 1).getValue();
        int    maxFontSize = 120;
        int    minFontSize = 10;
        double scale       = (double) (maxFontSize - minFontSize) / (maxFreq - minFreq);

        String html = wordFreqs.stream()
                .map(pair -> String.format(
                        "<span style='font-size: %dpx; color: %s;'>%s</span>",
                        (int) (scale * pair.getValue()) + minFontSize,
                        mkColor(),
                        pair.getKey())
                )
                .sorted((_left, _right) -> r.nextBoolean() ? -1 : +1)
                .collect(Collectors.joining(System.lineSeparator(), htmlStart, htmlEnd));


        Path outPath = Paths.get("./out/" + name + ".html");
        try (BufferedWriter out = Files.newBufferedWriter(outPath)) {
            out.write(html);
        }
        System.out.printf("Written file %s%n", outPath);
    }

    private String basename(Path path) {
        String file = path.getFileName().toString();
        return file.substring(0, file.lastIndexOf('.'));
    }

    private String mkColor() {
        return String.format("#%02X%02X%02X", r.nextInt(256), r.nextInt(256), r.nextInt(256));
    }

    private final Random r = new Random();

}