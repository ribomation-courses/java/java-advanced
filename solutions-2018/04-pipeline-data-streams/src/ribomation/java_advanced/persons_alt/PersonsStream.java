package ribomation.java_advanced.persons_alt;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

public class PersonsStream {
    public static void main(String[] args) throws Exception {
        PersonsStream app = new PersonsStream();
        app.run();
    }

    void run() throws IOException {
        String path = "./data/persons-data.alt-many.csv";

        Files.lines(Paths.get(path))
                .skip(1)
                .map(csv -> csv.split(","))
                .filter(fields -> fields[1].equals("Female"))
                .filter(fields -> {
                    int age = Integer.parseInt(fields[2]);
                    return 30 <= age && age <= 40;
                })
                .filter(fields -> {
                    int postCode = Integer.parseInt(fields[3]);
                    return postCode < 20000;
                })
                //.peek(fields -> System.out.println(Arrays.toString(fields)))
                .forEach(fields -> {
                    System.out.printf("%s, %s (%s)%n", fields[0], fields[2], fields[3]);
                });
        ;
    }

}
