package ribomation.java_advanced.persons;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.stream.Collectors;

public class PersonsApp {
    public static void main(String[] args) throws Exception {
        System.out.println(new PersonsApp().run("./data/persons-data.csv"));
    }

    String run(String csvFile) throws IOException {
        return Files.lines(Paths.get(csvFile))
                .skip(1)
                .map(Person::fromCSV)
                .filter(Person::isFemale)
                .filter((p) -> 30 <= p.getAge() && p.getAge() <= 40)
                .filter((p) -> p.getPostCode() < 20_000)
                .map(Person::toString)
                .collect(Collectors.joining(System.lineSeparator()))
                ;
    }
}
