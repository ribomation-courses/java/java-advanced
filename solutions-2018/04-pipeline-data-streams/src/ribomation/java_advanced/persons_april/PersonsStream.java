package ribomation.java_advanced.persons_april;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

public class PersonsStream {
    public static void main(String[] args) throws IOException {
        Files
                .lines(Paths.get("./data/persons-data-april.csv"))
                .skip(1)
                //.limit(100)
                .filter(line -> line.contains("Female"))
                .filter(line -> {
                    String[] fields = line.split(",");
                    int age = Integer.parseInt(fields[2]);
                    return (30 <= age) && (age <= 40);
                })
                .filter(line -> {
                    String[] fields = line.split(",");
                    int postCode = Integer.parseInt(fields[3]);
                    return postCode < 20_000;
                })
                //.peek(System.out::println)
                .forEach(System.out::println);
    }
}
