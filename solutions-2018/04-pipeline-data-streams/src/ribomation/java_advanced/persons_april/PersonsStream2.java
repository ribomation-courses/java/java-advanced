package ribomation.java_advanced.persons_april;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;
import java.util.stream.Collectors;

public class PersonsStream2 {

    static class Person {
        private String  name;
        private boolean female;
        private int     age;
        private int     postCode;

        public static Person fromCSV(String csv) {
            return fromCSV(csv, ",");
        }

        public static Person fromCSV(String csv, String sep) {
            String[] fields = csv.split(sep);
            return new Person(fields[0], fields[1], fields[2], fields[3]);
        }

        public Person(String name, String female, String age, String postCode) {
            this(name, female.equals("Female"), Integer.parseInt(age), Integer.parseInt(postCode));
        }

        public Person(String name, boolean female, int age, int postCode) {
            this.name = name;
            this.female = female;
            this.age = age;
            this.postCode = postCode;
        }

        @Override
        public String toString() {
            return "Person{" +
                    "name='" + name + '\'' +
                    ", female=" + female +
                    ", age=" + age +
                    ", postCode=" + postCode +
                    '}';
        }

        public String getName() {
            return name;
        }

        public boolean isFemale() {
            return female;
        }

        public int getAge() {
            return age;
        }

        public int getPostCode() {
            return postCode;
        }
    }

    public static void main(String[] args) throws IOException {
        List<Person> personList = Files
                .lines(Paths.get("./data/persons-data-april.csv"))
                .skip(1)
                .map(Person::fromCSV)
                .filter(Person::isFemale)
                .filter(p -> p.getPostCode() < 20_000)
                .filter(p -> (30 <= p.getAge()) && (p.getAge() <= 40))
                .collect(Collectors.toList());

        personList.forEach(System.out::println);
    }
}
