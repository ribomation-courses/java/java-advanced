package ribomation.java_advanced;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.util.*;
import java.util.concurrent.*;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static java.util.stream.Collectors.counting;
import static java.util.stream.Collectors.groupingBy;

public class WordFreqsAlt {
    public static void main(String[] args) throws Exception {
        new WordFreqsAlt().run(args);
    }

    void run(String[] args) throws Exception {
        var file     = "/shakespeare.txt";
        var chunk    = 5 * 1024;
        var minSize  = 5;
        var maxWords = 100;

        for (String arg : args) {
            if (arg.startsWith("--file=")) {
                file = arg.substring(7);
            } else if (arg.startsWith("--chunk=")) {
                chunk = Integer.parseInt(arg.substring(8)) * 1024;
            } else if (arg.startsWith("--min=")) {
                minSize = Integer.parseInt(arg.substring(6));
            } else if (arg.startsWith("--max=")) {
                maxWords = Integer.parseInt(arg.substring(6));
            }
        }

        run(file, chunk, minSize, maxWords);
    }

    void run(String bookPath, int chunkSize, int minWordSize, int maxWords) throws Exception {
        var is        = openStream(bookPath);
        var tasks     = createTasks(minWordSize, chunkSize, new InputStreamReader(is));
        var pool      = Executors.newFixedThreadPool(Runtime.getRuntime().availableProcessors());
        var results   = pool.invokeAll(tasks);
        var wordFreqs = aggregate(results);
        print(maxWords, wordFreqs);
        shutdown(pool);
    }

    InputStream openStream(String bookPath) {
        var is = getClass().getResourceAsStream(bookPath);
        if (is == null) {
            throw new IllegalArgumentException("cannot open resoruce: " + bookPath);
        }
        System.out.printf("Opened file %s%n", bookPath);
        return is;
    }

    List<Callable<Map<String, Long>>> createTasks(int minWordSize, int chunkSize, Reader in) {
        return Stream.generate(() -> Optional.ofNullable(createTask(minWordSize, chunkSize, in)))
                .takeWhile(Optional::isPresent)
                .map(Optional::get)
                .collect(Collectors.toList())
                ;
    }

    char[] readChunk(int chunkSize, Reader in) {
        try {
            var buf      = new char[chunkSize];
            var numBytes = in.read(buf);
            if (numBytes < 0) return null;
            if (numBytes < chunkSize) {
                return Arrays.copyOf(buf, numBytes);
            }
            return buf;
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    Callable<Map<String, Long>> createTask(int minWordSize, int chunkSize, Reader in) {
        var buf = readChunk(chunkSize, in);
        if (buf == null) return null;

        return () -> Pattern.compile("[^a-zA-Z]+")
                .splitAsStream(new String(buf, 0, buf.length))
                .filter(word -> word.length() >= minWordSize)
                .collect(groupingBy(String::toLowerCase, counting()))
                ;
    }

    Map<String, Long> aggregate(List<Future<Map<String, Long>>> results) throws Exception {
        var total = new HashMap<String, Long>();
        for (var r : results) {
            r.get().forEach((word, freq) -> {
                total.put(word, freq + total.getOrDefault(word, 0L));
            });
        }
        return total;
    }

    void print(int max, Map<String, Long> f) {
        System.out.printf("The %d most frequent words%n", max);
        f.entrySet().stream()
                .sorted((left, right) -> Long.compare(right.getValue(), left.getValue()))
                .limit(max)
                .forEach((p) -> {
                    System.out.printf("%12s: %d%n", p.getKey(), p.getValue());
                });
    }

    void shutdown(ExecutorService pool) {
        pool.shutdown();
        try {
            if (pool.awaitTermination(1, TimeUnit.MINUTES)) return;
        } catch (InterruptedException ignore) {
        }
        pool.shutdownNow();
    }
}
