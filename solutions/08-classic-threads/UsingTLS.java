import java.util.concurrent.atomic.AtomicInteger;

public class UsingTLS {
    static class Worker extends Thread {
        private static final AtomicInteger idCnt = new AtomicInteger(1);
        private static final ThreadLocal<Integer> id = new ThreadLocal<Integer>() {
            protected Integer initialValue() {
                return idCnt.getAndIncrement();
            }
        };
        public int myId() { return id.get(); }
        public void run() {
            delay(1);
            System.out.printf("Thread ID=%d%n", myId());
            delay(1);
        }
        private void delay(int n) {
            try { sleep(n * 1000); } catch (InterruptedException x) { }
        }
    }

    public static void main(String[] args) throws InterruptedException {
        var N = args.length == 0 ? 5 : Integer.parseInt(args[0]);

        var workers = new Worker[N];
        for (var k=0; k<N; ++k) workers[k] = new Worker();
        for (var w : workers) w.start();
        for (var w : workers) w.join();
    }
}

