# Class-Loader Demo

## Compile & Execute
Use the provided shell script to compile and run the
program.

    ./compile-and-run.sh

Study the execution print-outs and the program.
Modify the shell script to launch with a different number
of class-loader and objects per loader.
