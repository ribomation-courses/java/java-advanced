
import java.io.IOException;
import java.net.URL;
import java.net.URLClassLoader;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.*;

import static java.nio.file.Files.createTempDirectory;
import static java.nio.file.StandardCopyOption.REPLACE_EXISTING;

public class DuplicateGlobals {
    public static void main(String[] args) throws Exception {
        var classDir      = args.length >= 1 ? args[0] : "./classes";
        var numLoaders    = args.length >= 2 ? Integer.parseInt(args[1]) : 4;
        var objsPerLoader = args.length >= 3 ? Integer.parseInt(args[2]) : 2;

        var app       = new DuplicateGlobals();
        var deployDir = createTempDirectory("duplicate-globals");
        app.moveTargetClassFile(Path.of(classDir), deployDir);

        var loaders = app.createClassLoaders(numLoaders, deployDir);
        var objects = app.createObjects(loaders, objsPerLoader);

        System.out.println("-".repeat(40));
        app.dump(objects);

        System.out.println("-".repeat(40));
        app.shake(objects);

        System.out.println("-".repeat(40));
        app.dump(objects);

    }

    void moveTargetClassFile(Path classDir, Path deployDir) throws IOException {
        var classFile = classDir.resolve("Target.class");
        if (!Files.isReadable(classFile)) {
            throw new IllegalArgumentException("cannot read class file: " + classFile);
        }

        Files.createDirectories(deployDir.getParent());
        Files.move(classFile, deployDir.resolve(classFile.getFileName()), REPLACE_EXISTING);

        System.out.printf("Moved target class to %s%n", deployDir);
    }

    Map<String, ClassLoader> createClassLoaders(int n, Path dir) {
        var loaders = new TreeMap<String, ClassLoader>();
        var prefix  = 'A';
        while (n-- > 0) {
            loaders.put(prefix++ + "-", createClassLoader(dir));
        }
        return loaders;
    }

    ClassLoader createClassLoader(Path dir) {
        try {
            URL[] dirs = new URL[]{dir.toUri().toURL()};
            return new URLClassLoader(dirs);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    Set<Runnable> createObjects(Map<String, ClassLoader> loaders, int n) {
        var objects = new TreeSet<Runnable>();
        loaders.forEach((prefix, loader) -> {
            objects.addAll(createObjects(loader, prefix, n));
        });
        return objects;
    }

    Collection<Runnable> createObjects(ClassLoader loader, String prefix, int n) {
        var objects = new ArrayList<Runnable>();
        while (n-- > 0) {
            objects.add(createObject(loader, prefix + (n + 1)));
        }
        return objects;
    }

    Runnable createObject(ClassLoader loader, String id) {
        try {
            var cls  = loader.loadClass("Target");
            var cons = cls.getConstructor(String.class);
            var obj  = cons.newInstance(id);
            System.out.printf("created object: %s%n", obj);
            return (Runnable) obj;
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    void dump(Collection<Runnable> objs) {
        for (var obj : objs) System.out.printf("** %s%n", obj);
    }

    void shake(Collection<Runnable> objs) {
        for (var obj : objs) obj.run();
    }

//    void run() throws Exception {
//        moveTarget();
//        List<Runnable> objects = createObjects();
//        dump(objects);
//        System.out.println("--- shake() ---");
//        shake(objects);
//        System.out.println("--- dump() ---");
//        dump(objects);
//    }


}
