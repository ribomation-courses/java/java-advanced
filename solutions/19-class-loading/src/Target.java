
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Random;

public class Target implements Runnable, Comparable {
    private static int staticNumber = 42;
    private final String name;

    static {
        System.out.printf("*** Loaded class %s, ID=%d%n",
                Target.class.getName(),  Target.class.hashCode());
    }

    public Target(String name) {
        this.name = name;
    }

    @Override
    public int compareTo(Object obj) {
        try {
            String lhs  = (String) this.getClass().getMethod("getName").invoke(this);
            String rhs  = (String) obj.getClass().getMethod("getName").invoke(obj);
            return lhs.compareTo(rhs);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public String toString() {
        return String.format("%s{%s, number=%2d, id=%d}",
                getClass().getSimpleName(),
                name,
                staticNumber,
                getClass().hashCode()
        );
    }

    @Override
    public void run() {
        System.out.printf("BEFORE %s%n", this);
        shake();
        System.out.printf("AFTER  %s%n", this);
    }

    private void shake() {
        staticNumber = new Random(System.nanoTime() % 97).nextInt(100);
    }

    public String getName() {
        return name;
    }

    public static int getStaticNumber() {
        return staticNumber;
    }
}
