#!/usr/bin/env bash
set -eux

CLASS_DIR=./classes

rm -f $CLASS_DIR/*
javac -d $CLASS_DIR ./src/*.java
java -cp $CLASS_DIR DuplicateGlobals $CLASS_DIR 5 3

