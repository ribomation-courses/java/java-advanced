// RUN in JShell:
// $ jshell
// jshell> /open fibonacci.jsh

long fib(int n) {
  if (n < 0) throw new IllegalArgumentException("negative value");
  if (n == 0) return 0;
  if (n == 1) return 1;
  return fib(n-2) + fib(n-1);
}

int arg = 42;
System.out.printf("fib(%d) = %d%n", arg, fib(arg));
