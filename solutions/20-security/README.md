# Sandbox Demo

## Compile & Run
Run it as a single source Java app.

### Without a Security Manager
Should print out the contents of `./data/simple-file.txt`

    java src/FileAccessDemo.java

### With a Security Manager
Should result in a `java.security.AccessControlException`

    java src/FileAccessDemo.java true

### With a Security Manager and a permissive policy
Should now work again.

    java src/FileAccessDemo.java true true
    
