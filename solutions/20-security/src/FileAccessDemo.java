import java.nio.file.Files;
import java.nio.file.Path;

public class FileAccessDemo {
    public static void main(String[] args) {
        var useSecurityMgr = args.length >= 1 && Boolean.parseBoolean(args[0]);
        var usePolicyFile  = args.length >= 2 && Boolean.parseBoolean(args[1]);

        if (usePolicyFile) {
            System.setProperty("java.security.policy", "./cfg/simple.policy");
        }
        if (useSecurityMgr) {
            System.setSecurityManager(new SecurityManager());
        }

        readFile(Path.of("./data/simple-file.txt"));
    }

    static void readFile(Path file) {
        System.out.printf("--- Reading %s ---%n", file);
        try (var in = Files.newBufferedReader(file)) {
            in.lines().forEach(System.out::println);
        } catch (Exception e) {
            System.out.printf("Error: %s%n", e);
        }
    }
}
