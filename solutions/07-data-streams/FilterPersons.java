// compile & run: java FilterPersons.java

import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.stream.Collectors;

public class FilterPersons {
    static class Person {
        String name;
        boolean female;
        int age;
        int postCode;

        Person(String[] fields) {
            name = fields[0];
            female = fields[1].equals("Female");
            age = Integer.parseInt(fields[2]);
            postCode = Integer.parseInt(fields[3]);
        }

        public String toString() {
            return String.format("Person[%s, age %d, %s, code %d]", 
            name, age, female?"female":"male", postCode);
        }

        public boolean isFemale() {return female;}
        public int getAge() {return age;}
        public int getPostCode() {return postCode;}
    }

    public static void main(String[] args) throws Exception {
        var filename = "../../files/many-persons.csv";

        var result =
        Files.lines(Paths.get(filename))
        .skip(1)
        .map(csv -> csv.split(","))
        .map(fields -> new Person(fields))
        .filter(Person::isFemale)
        .filter(p -> 30 <= p.getAge() && p.getAge() <= 40)
        .filter(p -> p.getPostCode() < 20_000)
        .collect(Collectors.toList());

        result.forEach(System.out::println);
    }
}
