// compile & run: java WordCloud.java

import java.nio.file.Path;
import java.nio.file.Files;
import java.util.regex.Pattern;
import java.util.Random;
import java.util.Map;

import static java.util.stream.Collectors.groupingBy;
import static java.util.stream.Collectors.counting;
import static java.util.stream.Collectors.toList;
import static java.util.stream.Collectors.joining;
import static java.util.Comparator.comparingLong;
import static java.lang.String.format;

public class WordCloud {
    public static void main(String[] args) throws Exception {
        var filename  = args.length == 0 ? "../../files/musketeers.txt" : args[0];
        var minLength = 4;
        var maxWords  = 100;

        var start = System.nanoTime();
        var html  = new WordCloud().run(Path.of(filename), minLength, maxWords);
        var stop  = System.nanoTime();

        var outfile = Path.of("word-cloud.html");
        try (var out = Files.newBufferedWriter(outfile)) {
            out.write(html);
        }
        System.out.printf("Written %s%n", outfile);
        System.out.printf("Elapsed %.3f seconds%n", (stop - start) * 1E-9);
    }

    String run(Path path, int minLength, int maxWords) throws Exception {
        var wordFreqs = 
        Files.lines(path)
        .flatMap(whiteSpace::splitAsStream)
        .filter(word -> word.length() >= minLength)
        .collect(groupingBy(String::toLowerCase, counting()))
        .entrySet()
        .stream()
        .sorted(comparingLong(Map.Entry<String, Long>::getValue).reversed())
        .limit(maxWords)
        .collect(toList());

        var maxFreq = wordFreqs.get(0).getValue();
        var minFreq = wordFreqs.get(wordFreqs.size() - 1).getValue();
        var scale   = (double) (maxFontSize - minFontSize) / (maxFreq - minFreq);

        return wordFreqs.stream()
            .map(wf -> format("<span style='font-size: %dpx; color: %s;'>%s</span>",  
                (int) (scale * wf.getValue()) + minFontSize,
                mkColor(),
                wf.getKey()
            ))
            .sorted((_left, _right) -> r.nextBoolean() ? -1 : +1)
            .collect(joining(System.lineSeparator(), htmlPrefix, htmlSuffix));
    }

    final Pattern   whiteSpace  = Pattern.compile("[^a-zA-Z]+");
    final int       maxFontSize = 130;
    final int       minFontSize = 15;
    final Random    r           = new Random();

    String mkColor() {
        return String.format("#%02X%02X%02X", r.nextInt(256), r.nextInt(256), r.nextInt(256));
    }

    final String htmlPrefix = "<!DOCTYPE html>\n" 
        + "<html lang=\"en\"><head>" 
        + "<title>Word Cloud</title>" 
        + "<style>"
        + "body {font-family: sans-serif;} "
        + "h1 {text-align:center; font-size:30pt; color:white; background-color:green; margin-top:0;} "
        + "</style>"
        + "</head><body>"
        + "<h1>Word Cloud</h1>";
    final String htmlSuffix = "</body></html>";
}