// compile & run: java ClimateData.java

import  java.nio.file.Path;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.Map;
import java.util.zip.GZIPInputStream;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

import static java.nio.file.Files.isReadable;
import static java.nio.file.Files.newInputStream;
import static java.util.stream.Collectors.averagingDouble;
import static java.util.stream.Collectors.groupingBy;
import static java.util.Comparator.comparingInt;

public class ClimateData {
    public static void main(String[] args) throws Exception {
        var filename = "../../../../../../large-files/climate-data.txt.gz"; //CHANGE THIS PATH

        var path = Path.of(filename);
        if (!isReadable(path)) {
            throw new RuntimeException("cannot read file: " + path);
        }

        System.out.printf("Started processing %s ...%n", path.getFileName());
        var start = System.nanoTime();
        new ClimateData().run(path);
        var stop = System.nanoTime();
        System.out.printf("Elapsed %.3f seconds", (stop - start) * 1E-9);
    }

    void run(Path path) throws Exception {
        var reader = 
            new BufferedReader(
                new InputStreamReader(
                    new GZIPInputStream(
                        newInputStream(path))));
        try (reader) {
            reader.lines().parallel()
            .filter(line -> !line.startsWith("STN--"))
            .map(line -> {
                String[] fields = line.split("\\s+");
                return new Pair<String,String>(fields[2], fields[3]);
            })
            .map(p -> {
                var date       = LocalDate.parse(p.first, DateTimeFormatter.BASIC_ISO_DATE);
                var fahrenheit = Double.parseDouble(p.second);
                var celsius    = (5D / 9D) * (fahrenheit - 32D);
                return new Pair<LocalDate,Double>(date, celsius);
            })
            .collect(groupingBy(p -> p.first.getYear(), averagingDouble(p -> p.second.doubleValue())))
            .entrySet()
            .parallelStream()
            .sorted(comparingInt(Map.Entry<Integer,Double>::getKey))
            .forEach(data -> System.out.printf("%d: %.2f C%n", data.getKey(), data.getValue()));
        }
    }

    static class Pair<First, Second> {
        First first;
        Second second;
        Pair(First f, Second s) {first=f; second=s;}
    }
}
