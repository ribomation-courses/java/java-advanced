import java.nio.file.Path;
import java.util.Locale;
import java.util.concurrent.ForkJoinPool;
import java.util.concurrent.ForkJoinTask;
import java.util.concurrent.RecursiveTask;
import java.util.stream.Stream;
import java.io.File;
import java.nio.file.Files;

public class DirSize {
    public static void main(String[] args) throws Exception {
        var dirname = args.length==0 ? "../" : args[0];

        var path = Path.of(dirname);
        if (!Files.isDirectory(path)) {
            throw new RuntimeException("not a directory: " + path);
        }

        var start  = System.nanoTime();
        var result = new ForkJoinPool().invoke(new SizeTask(path.toFile())); 
        var stop   = System.nanoTime();

        System.out.printf(Locale.ENGLISH,
                "DIR: %s has%n %,d files of a total of %,.3f GB (elapsed %.3f secs)%n",
                path.toAbsolutePath(),
                result.numFiles,
                result.numBytes / (1024 * 1024 * 1024.0),
                (stop - start) * 1E-9
        );
    }

    static class SizeTask extends RecursiveTask<Result> {
        File dir;

        SizeTask(File dir) {
            this.dir = dir;
            //System.out.printf("Task: %s%n",dir);
        }

        @Override
        protected Result compute() {
            var result = new Result();
            return Stream.of(dir.listFiles())
            .peek(e -> {
                if (e.isFile()) {
                    result.add(e.length());
                }
            })
            .filter(File::isDirectory)
            .map(d -> new SizeTask(d).fork())
            .map(ForkJoinTask::join)
            .reduce(result, Result::add);
        }
    }

    static class Result {
        long numFiles = 0;
        long numBytes = 0;

        public Result() { }

        public Result add(long length) {
            this.numFiles++;
            this.numBytes += length;
            return this;
        }

        public Result add(Result r) {
            this.numFiles += r.numFiles;
            this.numBytes += r.numBytes;
            return this;
        }
    }

}
