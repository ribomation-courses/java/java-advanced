// N.B. This Java program only runs on Linux and similar, i.e. not on GIT Bash
// Compile & run: java ShellPipe.java

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.Arrays;
import java.util.List;

public class ShellPipe {
    public static void main(String[] args) throws Exception {
        var dir        = args.length>=1 ? args[0] : "../";
        var maxResults = args.length>=2 ? Integer.parseInt(args[1]) : 10;
        fileStats(dir, maxResults);
    }

    // find .. -type f -name '*.java' | xargs wc | grep -v total | sort -nr -k 2 | head -5
    static void fileStats(String dir, int maxResults) throws Exception {
        ProcessBuilder[] steps = { 
            new ProcessBuilder("find", dir, "-type", "f", "-name", "*.java"),
            new ProcessBuilder("xargs", "wc"), 
            new ProcessBuilder("grep", "-v", "total"),
            new ProcessBuilder("sort", "-nr", "-k", "2"), 
            new ProcessBuilder("head", "-" + maxResults)
        };

        List<Process> pipeline = ProcessBuilder.startPipeline(Arrays.asList(steps));
        Process last = pipeline.get(pipeline.size() - 1);

        try (var in = new BufferedReader(new InputStreamReader(last.getInputStream()))) {
            in.lines().forEach(System.out::println);
        }
    }
}
