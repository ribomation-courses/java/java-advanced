
public class ProducerConsumer {
    static class MailBox<T> {
        private T payload;
        private boolean empty = true;

        public synchronized void put(T x) {
            try {
                while (empty == false) wait();
                payload = x;
                empty = false;
            } catch (Exception err) {
                throw new RuntimeException(err);
            } finally {
                notifyAll();
            }
        }

        public synchronized T get() {
            try {
                while (empty == true) wait();
                empty = true;
                return payload;
            } catch (Exception err) {
                throw new RuntimeException(err);
            } finally {
                notifyAll();
            }
        }   
    }

    public static void main(String[] args) throws Exception {
        final var N  = args.length == 0 ? 100 : Integer.parseInt(args[0]);
        final var mb = new MailBox<String>();

        var sender = new Thread(() -> {
            for (var k=1; k<=N; ++k) mb.put(String.format("#%05X", k));
            mb.put(null);
        });

        var receiver = new Thread(() -> {
            for (var msg = mb.get(); msg != null; msg = mb.get())
                System.out.printf("Consumer: %s%n", msg);
        });

        receiver.start();
        sender.start();
        sender.join();
        receiver.join();
    }
}
