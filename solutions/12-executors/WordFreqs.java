import java.io.IOException;
import java.io.Reader;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Optional;
import java.util.Map;
import java.util.HashMap;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.regex.Pattern;
import java.util.stream.Stream;

import static java.util.stream.Collectors.groupingBy;
import static java.util.stream.Collectors.counting;
import static java.util.concurrent.Executors.newFixedThreadPool;
import static java.util.Comparator.comparingLong;

public class WordFreqs {
    public static void main(String[] args) throws Exception {
        final var filename  = "../../files/musketeers.txt";
        final var chunkSize = 10_000;
        final var P         = Runtime.getRuntime().availableProcessors();

        var pool = newFixedThreadPool(P);
        var in = Files.newBufferedReader(Path.of(filename));
        try (in) {
            Stream
            .generate(() -> createTask(in, chunkSize))
            .takeWhile(Optional::isPresent)
            .map(Optional::get)
            .map(task -> pool.submit(task))
            .map(r -> {
                try {
                    return r.get();
                } catch (InterruptedException | ExecutionException e) { }
                return null;
            })
            .collect(
                HashMap<String, Long>::new, 
                (total, partial) -> {
                    partial.forEach((w, f) -> {
                        total.put(w, f + total.getOrDefault(w, 0L));
                    });
                }, 
                (m1, m2) -> m1.putAll(m2))
            .entrySet()
            .stream()
            .sorted(comparingLong(Map.Entry<String, Long>::getValue).reversed())
            .limit(25)
            .forEach(pair -> System.out.printf("%s: %d%n", pair.getKey(), pair.getValue()));
        } finally {
            pool.shutdownNow();
        }
    }

    static Optional<Callable<Map<String, Long>>> createTask(Reader in, int N) {
        var payload = readChunk(in, N);
        if (payload.isEmpty()) return Optional.empty();

        return Optional.of(() -> {
            return Pattern
            .compile("[^a-zA-Z]+")
            .splitAsStream(payload.get())
            .filter(w -> w.length() >= 4)
            .collect(groupingBy(String::toLowerCase, counting()));
        });
    }

    static Optional<String> readChunk(Reader in, int chunkSize) {
        try {
            var buf      = new char[chunkSize];
            var numBytes = in.read(buf);
            if (numBytes > 0) return Optional.of(new String(buf, 0, numBytes));
        } catch (IOException e) { }
        return Optional.empty();
    }

}
