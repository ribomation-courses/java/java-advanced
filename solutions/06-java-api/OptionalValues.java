
import java.util.ArrayList;
import java.util.Optional;

public class OptionalValues {
    class OptList<T> extends ArrayList<T> {
        public Optional<T> lookup(int idx) {
            try {
                return Optional.ofNullable(super.get(idx));
            } catch (IndexOutOfBoundsException e) {
                return Optional.empty();
            }
        }
    }

    OptList<String> mk(int n) {
        OptList<String> lst = new OptList<>();
        for (int k = 0; k < n; ++k)
            lst.add(String.format("str-%d", k));
        return lst;
    }

    void run(int N) {
        OptList<String> list = mk(N);

        System.out.printf("list[%d] = %s%n", N - 2,
                list.lookup(N - 2).orElse("nope"));

        System.out.printf("list[%d] = %s%n", N + 2,
                list.lookup(N + 2).orElse("nope"));
    }

    public static void main(String[] args) {
        new OptionalValues().run(5);
    }
}