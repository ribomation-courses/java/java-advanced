#!/c/Users/jensr/.sdkman/candidates/java/current/bin/java --source 12
/*
	Compile & Run: ./numbers.sh
*/

import java.util.*;
import static java.lang.System.Logger.Level.INFO;

public class NumbersApp {
	public static void main(String[] args) {
        var n = 10;
        if (args.length > 0) n = Integer.parseInt(args[0]);
		var app = new NumbersApp();
		app.run(n);
	}
	
	void run(int N) {
		System.Logger out = System.getLogger("numbers");
		
		var lst = new ArrayList<Integer>();
		for (int k=1; k<=N; ++k) lst.add(k);
		out.log(INFO, "lst: " + lst);
		
		var lst2 = new ArrayList<Double>();
		for (var n : lst) lst2.add(n * Math.PI);
		out.log(INFO, "lst2: " + lst2);
		
		var lst3 = new ArrayList<String>();
		for (var n : lst2) lst3.add(String.format("str = %.2f", n));
		out.log(INFO, "lst3: " + lst3);
	}
}

