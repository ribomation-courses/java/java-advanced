/*
    Compile: javac --release 12 --enable-preview SimpleSwitch.java
    Run    : java --enable-preview SimpleSwitch
*/

import java.util.stream.IntStream;

public class SimpleSwitch {
    public static void main(String[] args) {
        SimpleSwitch app = new SimpleSwitch();
        app.run();
    }

    private void run() {
        IntStream.rangeClosed(0, 8)
                .forEach(n -> System.out.printf("%d -> %s%n", n, num2str(n)));
    }

    private String num2str(int num) {
        return switch (num) {
            case 0 -> "Zero";
            case 1 -> "One";
            case 2 -> "Two";
            case 3, 4, 5 -> "Few";
            default -> "Many";
        };
    }
}


