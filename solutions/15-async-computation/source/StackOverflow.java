
import org.jsoup.Jsoup;
import org.jsoup.nodes.Element;

import java.io.IOException;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static java.lang.String.format;
import static java.lang.System.lineSeparator;
import static java.nio.file.Files.lines;
import static java.nio.file.Files.newBufferedWriter;
import static java.util.stream.Collectors.joining;


public class StackOverflow {
    public static void main(String[] args) {
        var tagsFile  = "./input/tags.txt";
        var tmplFile  = "./input/results.html";
        var htmlFile  = "./output/stack-overflow.html";
        var soBaseUrl = "https://stackoverflow.com/questions/tagged/";
        var numTopics = 5;
        var pageTitle = "Most Popular Questions of StackOverflow";

        new StackOverflow()
                .run(tagsFile, tmplFile, htmlFile, numTopics, soBaseUrl, pageTitle);
    }

    void run(String tagsFile, String tmplFile, String htmlFile, int numTopics, String soBaseUrl, String title) {
        var start = System.nanoTime();

        CompletableFuture
                .supplyAsync(() -> load(tagsFile).map(tag -> fetch(tag, numTopics, soBaseUrl)))
                .thenApply(promises -> promises.map(CompletableFuture::join))
                .thenApply(results -> asHTML(results, load(tmplFile).collect(joining(lineSeparator())), title))
                .thenAccept(html -> {
                    var  elapsed = (System.nanoTime() - start) * 1E-9;
                    Path file    = store(htmlFile, substitute(html, "ELAPSED", format("%.3f", elapsed)));
                    System.out.printf("Written: %s%nElapsed time: %.3f seconds%n", file, elapsed);
                })
                .exceptionally(err -> {
                    err.printStackTrace();
                    return null;
                })
                .join();
    }

    static class Result {
        String       tag;
        List<String> topics = new ArrayList<>();

        Result(String tag) {
            this.tag = tag;
        }
    }

    CompletableFuture<Result> fetch(String tag, int numTopics, String baseUrl) {
        return CompletableFuture.supplyAsync(() -> {
            Result r = new Result(tag);
            try {
                r.topics = Jsoup
                        .connect(baseUrl + tag)
                        .get()
                        .select("a.question-hyperlink")
                        .stream()
                        .map(Element::text)
                        .limit(numTopics)
                        .collect(Collectors.toList());
                return r;
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
        });
    }

    String asHTML(Stream<Result> results, String html, String title) {
        return Stream.of(html)
                .map(line -> substitute(line, "TITLE", title))
                .map(line -> substitute(line, "DATE", format("%1$tF %1$tR", new Date())))
                .map(line -> substitute(line, "BODY", paragraphs(results)))
                .collect(joining());
    }

    String substitute(String txt, String key, String value) {
        return txt.replace("{{" + key + "}}", value);
    }

    String paragraphs(Stream<Result> results) {
        return results.map(res -> H3(res.tag) + UL(res.topics)).collect(joining());
    }

    String H3(String txt) {
        return format("<h3 class=\"bg-success text-white p-1\">Tag: %s</h3>%n", txt);
    }

    String UL(List<String> topics) {
        return "<ul>" + lineSeparator()
                + topics.stream().map(this::LI).collect(joining())
                + "</ul>" + lineSeparator();
    }

    String LI(String txt) {
        return format("<li>%s</li>%n", txt);
    }

    Stream<String> load(String filename) {
        try {
            return lines(Path.of(filename));
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    Path store(String filename, String content) {
        var path = Path.of(filename);
        try (var out = newBufferedWriter(path)) {
            out.write(content);
            return path;
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

}
