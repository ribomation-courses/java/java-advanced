
import java.util.*;
import java.util.concurrent.ThreadLocalRandom;
import static java.lang.String.format;

public class Bank {
    static class Account {
        final int accno;
        int balance = 0;
        Account(int accno) { this.accno = accno; }
        public String toString() {
            return format("Account{%d, %d kr}", accno, balance);
        }
    }

    static class AccountRepo {
        final List<Account> accounts;

        AccountRepo(int N) {
            accounts = new ArrayList<>();
            for (var k = 0; k < N; ++k)
                accounts.add( new Account(k+1) );
        }

        Account[] pickTwo() {
            Account a = accounts.get(nextIndex(accounts));
            Account b;
            do {
                b = accounts.get(nextIndex(accounts));
            } while (a.accno == b.accno);
            return new Account[] { a, b };
        }

        int nextIndex(List<Account> accounts) {
            return ThreadLocalRandom.current().nextInt(accounts.size());
        }
    }

    static class SafeAccountRepo extends AccountRepo {
        SafeAccountRepo(int N) {super(N);}

        @Override
        Account[] pickTwo() {
            var pair = super.pickTwo();
            if (pair[0].accno < pair[1].accno) {
                return pair;
            } else {
                return new Account[]{pair[1], pair[0]};
            }
        }
    }

    static void transfer(int id, Account from, Account to, int amount) {
        synchronized (from) {
            synchronized (to) {
                from.balance += -amount;
                to.balance += +amount;
                System.out.printf("[agent-%02d] %d kr: Acc-%02d -> Acc-%02d%n", 
                                id, amount, from.accno, to.accno);
            }
        }
    }

    public static void main(String[] args) throws Exception {
        var numAgents    = 25;
        var numAccounts  = numAgents / 2;
        var numTransfers = 100;
        var deadlock     = true;

        if (args.length > 0 && args[0].equals("no")) 
            deadlock = false;

        final var repo = deadlock
            ? new AccountRepo(numAccounts)
            : new SafeAccountRepo(numAccounts);

        var agents = new Thread[numAgents];
        for (var k=0; k<agents.length; ++k) {
            final var id = k+1;
            final var N  = numTransfers;
            agents[k] = new Thread(() -> {
                for (var t=0; t<N; ++t) {
                    var two = repo.pickTwo();
                    transfer(id, two[0], two[1], 100);
                }                
            });
        }
        for (var a : agents) a.start();
        for (var a : agents) a.join();
        
        System.out.printf("Total balance: %d kr%n",
            repo.accounts.stream()
            .mapToInt(a -> a.balance)
            .sum()
        );
    }
}
