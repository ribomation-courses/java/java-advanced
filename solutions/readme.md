Solutions
====
Here are the latest solutions for the exercises of the course.

All Java program code are written for Java 12,
although most of the can be run with Java 11.

The Java programs are all taking advantage of the new
feature introduced in Java 11, that allows the JVM to
compile a single source Java file on the fly and run 
it immediately. Just open one of the (solution) 
directories in a terminal verify that you indeed are 
running Java 11 or later

    $ java --version
    openjdk 12.0.1 2019-04-16
    OpenJDK Runtime Environment (build 12.0.1+12)
    OpenJDK 64-Bit Server VM (build 12.0.1+12, mixed mode, sharing)


Then _compile & run_ the program in one step

    java SomeJavaApp.java

