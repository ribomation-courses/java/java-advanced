// compile & run: java SimpleLambdas.java

import java.util.List;
import java.util.Arrays;
import java.util.ArrayList;

public class SimpleLambdas {
    // --- repeat ---
    public interface Repeat {
        void doit(int k);
    }

    public static void repeat(int count, Repeat expr) {
        for (var k = 1; k <= count; ++k)
            expr.doit(k);
    }

    void using_repeat() {
        System.out.print("Repeat: ");
        repeat(10, (k) -> System.out.printf("%s-%d ", "Java", k));
        System.out.println();
    }

    // --- apply ---
    public interface Apply<T> {
        T transform(T x);
    }

    public static <T> List<T> apply(List<T> list, Apply<T> f) {
        var result = new ArrayList<T>();
        for (T item : list)
            result.add(f.transform(item));
        return result;
    }

    void using_apply() {
        System.out.println("--- apply ---");
        var integers = Arrays.asList(1, 2, 3, 4, 5, 6, 7, 8, 9, 10);
        System.out.println("n   : " + integers);
        System.out.println("n*n : " + apply(integers, n -> n * n));
        System.out.println("tUC :" + apply(List.of("a","b","c"), String::toUpperCase));
    }

    // --- main ---
    public static void main(String[] args) {
        var app = new SimpleLambdas();
        app.using_repeat();
        app.using_apply();
    }
}
