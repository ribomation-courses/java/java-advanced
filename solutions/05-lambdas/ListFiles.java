// compile & run: java ListFiles.java

import java.io.File;
import java.io.IOException;
import java.util.Arrays;

public class ListFiles {
    public static void main(String[] args) throws Exception {
        var app    = new ListFiles();
        app.run(new File("."));
    }

    void run(File dir) throws IOException {
        if (!dir.isDirectory()) {
            throw new RuntimeException("not a directory: " + dir);
        }

        File[] files = dir.listFiles(f ->
                f.isFile()
             && f.getName().endsWith(".java")
             && f.length() > 600
        );
        assert files != null;

        Arrays.asList(files).forEach(System.out::println);
                                 // f -> System.out.println(f)
    }
}
