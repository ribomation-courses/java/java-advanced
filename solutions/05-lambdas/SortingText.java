
// compile & run: java SortingText.java

import java.util.Arrays;
import java.util.List;
import java.util.Comparator;

public class SortingText {
    public static void main(String[] args) {
        new SortingText().run();
    }

    void run() {
        String[] words = { "Java", "is", "a", "cool", "language", "!!!" };
        System.out.printf("Init: %s%n", Arrays.toString(words));
        System.out.printf("ASC : %s%n", Arrays.toString(sortLexicalAsc(words)));
        System.out.printf("DESC: %s%n", Arrays.toString(sortLexicalDesc(words)));
        System.out.printf("Size: %s%n", Arrays.toString(sortSizeDesc(words)));
        System.out.printf("Size: %s%n", Arrays.toString(sortSizeDesc2(words)));
    }

    String[] sortLexicalAsc(String[] words) {
        Arrays.sort(words, (left, right) -> left.compareTo(right));
        return words;
    }

    String[] sortLexicalDesc(String[] words) {
        Arrays.sort(words, (left, right) -> right.compareTo(left));
        return words;
    }

    String[] sortSizeDesc(String[] words) {
        Arrays.sort(words, (left, right) -> Integer.compare(right.length(), left.length()));
        return words;
    }

    String[] sortSizeDesc2(String[] words) {
        Arrays.sort(words, Comparator.comparingInt(String::length).reversed());
        return words;
    }

}
