# JVM Heap Demo

## VisualVM
Download [VisualVM](https://visualvm.github.io/download.html)
and unpack it to a folder. Then launch it.

Within the _Tools_ menu and the _Available Plugins_ tab, find the
`VisualGC` plugin and install it. Restart VisualVM.

## Compile & Run
Launch the swamper and connect to it via VisualVM and open its
VisualGC tab. 

    java -Xmx300M src/MemorySwamper.java

Try running with different heap sizes and watch behaviour in VisualVM.
Just remember to close the previous program tab and open a new one for
the program you just started. 

