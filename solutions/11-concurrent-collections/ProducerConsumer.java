import java.util.concurrent.*;

public class ProducerConsumer {
    interface Receivable<T> {
        void send(T x);
    }

    static class MessageThread<T> extends Thread implements Receivable<T> {
        private BlockingQueue<T> inbox;

        protected MessageThread(int maxMessages) {
            inbox = new ArrayBlockingQueue<>(maxMessages);
        }

        protected MessageThread() {
            this(10);
        }

        @Override
        public void send(T msg) {
            try {
                inbox.put(msg);
            } catch (InterruptedException ignore) {
            }
        }

        protected T recv() {
            try {
                return inbox.take();
            } catch (InterruptedException ignore) {
            }
            return null;
        }
    }

    static class Consumer extends MessageThread<Long> {
        @Override
        public void run() {
            for (var msg = recv(); msg > 0; msg = recv()) {
                System.out.printf("[consumer] %d%n", msg);
            }
        }
    }

    static class Producer extends Thread {
        private final int              maxNumber;
        private final Receivable<Long> next;
    
        public Producer(int maxNumber, Receivable<Long> next) {
            this.maxNumber = maxNumber;
            this.next = next;
        }
    
        @Override
        public void run() {
            for (long k = 1; k <= maxNumber; ++k) next.send(k);
            next.send(-1L);
        }
    }

    public static void main(String[] args) throws Exception {
        final var N = args.length==0 ? 10 : Integer.parseInt(args[0]);
        
        var c = new Consumer();
        c.start();

        var p = new Producer(N, c);
        p.start();

        p.join();
        c.join();
    }
}
