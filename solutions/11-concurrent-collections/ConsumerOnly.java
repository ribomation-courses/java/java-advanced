import java.util.concurrent.*;

public class ConsumerOnly {
    interface Receivable<T> {
        void send(T x);
    }

    static class MessageThread<T> extends Thread implements Receivable<T> {
        private BlockingQueue<T> inbox;

        protected MessageThread(int maxMessages) {
            inbox = new ArrayBlockingQueue<>(maxMessages);
        }

        protected MessageThread() {
            this(10);
        }

        @Override
        public void send(T msg) {
            try {
                inbox.put(msg);
            } catch (InterruptedException ignore) {
            }
        }

        protected T recv() {
            try {
                return inbox.take();
            } catch (InterruptedException ignore) {
            }
            return null;
        }
    }

    static class Consumer extends MessageThread<Long> {
        @Override
        public void run() {
            for (var msg = recv(); msg > 0; msg = recv()) {
                System.out.printf("[consumer] %d%n", msg);
            }
        }
    }
    
    public static void main(String[] args) throws Exception {
        final var N = args.length==0 ? 10 : Integer.parseInt(args[0]);
        var c = new Consumer();
        c.start();
        for (long k = 1; k <= N; ++k) c.send(k);
        c.send(-1L);
        c.join();
    }
}
