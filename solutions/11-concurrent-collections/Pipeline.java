import java.util.concurrent.*;

public class Pipeline {
    interface Receivable<T> {
        void send(T x);
    }

    static class MessageThread<T> extends Thread implements Receivable<T> {
        private BlockingQueue<T> inbox;

        protected MessageThread(int maxMessages) {
            inbox = new ArrayBlockingQueue<>(maxMessages);
        }

        protected MessageThread() {
            this(10);
        }

        @Override
        public void send(T msg) {
            try {
                inbox.put(msg);
            } catch (InterruptedException ignore) {
            }
        }

        protected T recv() {
            try {
                return inbox.take();
            } catch (InterruptedException ignore) {
            }
            return null;
        }
    }

    static class Consumer extends MessageThread<Long> {
        @Override
        public void run() {
            for (var msg = recv(); msg > 0; msg = recv()) {
                System.out.printf("[consumer] %d%n", msg);
            }
        }
    }

    static class Producer extends Thread {
        private final int              maxNumber;
        private final Receivable<Long> next;
    
        public Producer(int maxNumber, Receivable<Long> next) {
            this.maxNumber = maxNumber;
            this.next = next;
        }
    
        @Override
        public void run() {
            for (long k = 1; k <= maxNumber; ++k) next.send(k);
            next.send(-1L);
        }
    }

    static class Transformer extends MessageThread<Long> {
        private final Receivable<Long> next;
    
        public Transformer(Receivable<Long> next) {
            this.next = next;
        }
    
        @Override
        public void run() {
            long msg;
            while ((msg = recv()) > 0) {
                next.send(2 * msg);
            }
            next.send(-1L);
        }
    }

    public static void main(String[] args) throws Exception {
        var T = args.length>=1 ? Integer.parseInt(args[0]) : 5;
        var N = args.length>=2 ? Integer.parseInt(args[1]) : 10;

        var c = new Consumer();
        c.start();

        Receivable<Long> next = c;
        while (T-- > 0) {
            Transformer t = new Transformer(next);
            t.start();
            next = t;
        }

        var p = new Producer(N, next);
        p.start();

        c.join();
    }
}
