package se.ribomation.numbers;

import se.ribomation.internal.FunctionFactoryImpl;

import java.util.Collection;
import java.util.Collections;
import java.util.Optional;

public abstract class FunctionFactory {
    public static FunctionFactory instance;

    static {
        instance = new FunctionFactoryImpl();
    }

    private int multiplier = 10;

    public Collection<String> functionNames() {
        return Collections.singletonList("");
    }

    public int getMultiplier() {
        return multiplier;
    }

    public void setMultiplier(int multiplier) {
        this.multiplier = multiplier;
    }

    public Optional<NumberFunction> get(String name) {
        return Optional.empty();
    }

}
