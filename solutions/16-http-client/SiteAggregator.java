// Compile & run: java -cp jsoup-1.12.1.jar SiteAggregator.java

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CompletableFuture;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static java.net.http.HttpClient.Redirect.NORMAL;
import static java.net.http.HttpResponse.BodyHandlers.ofString;
import static java.time.Duration.ofSeconds;

public class SiteAggregator {
    public static void main(String[] args) {
        new SiteAggregator().pipeline("https://www.ribomation.com", "/index.html");
    }

    void pipeline(String baseUrl, String indexPageUrl) {
        System.out.printf("Fetching links from %s ...%n", indexPageUrl);
        long         startTime     = System.nanoTime();
        final long[] linkFetchTime = {0};
        final int[]  linkNo        = {1};
        client().sendAsync(GET(baseUrl + indexPageUrl), ofString())
                .thenApply(HttpResponse::body)
                .thenApply(htmlPage -> {
                    linkFetchTime[0] = System.nanoTime();
                    return htmlPage;
                })
                .thenApply(htmlPage ->
                        Jsoup.parse(htmlPage, baseUrl)
                                .select("#courses-list a.course").stream()
                                .map(link -> link.attr("href"))
                                .filter(url -> !url.endsWith("index.html"))
                                .map(uri -> baseUrl + uri)
                                .peek(url -> System.out.printf("[%d] %s%n", linkNo[0]++, url))
                                .map(url ->
                                        client().sendAsync(GET(url), ofString())
                                                .thenApply(HttpResponse::body)
                                                .thenApply(coursePage -> extractCourseInfo(coursePage, baseUrl))
                                )
                )
                .thenApply((Stream<CompletableFuture<Map<String, String>>> data) ->
                        data.map(CompletableFuture::join)
                )
                .thenApply((Stream<Map<String, String>> courses) -> {
                    List<Map<String, String>> list = courses.collect(Collectors.toList());
                    int maxWidth = list.stream()
                            .mapToInt(data -> data.get("name").length())
                            .max().orElse(25);
                    return list.stream()
                            .map(course ->
                                    String.format("%-" + maxWidth + "s (%s): %s",
                                            course.get("name"), course.get("duration"), course.get("ingress")))
                            .collect(Collectors.toList());
                })
                .thenApply(courseList -> {
                    courseList.forEach(System.out::println);
                    return courseList.size();
                })
                .thenAccept(numCourses -> {
                    printSummary(numCourses, System.nanoTime() - startTime, linkFetchTime[0] - startTime);
                })
                .join();
    }

    Map<String, String> extractCourseInfo(String coursePageHtml, String baseUrl) {
        Document doc      = Jsoup.parse(coursePageHtml, baseUrl);
        String   name     = doc.select("#facts th:contains(Name) + td").text();
        String   duration = doc.select("#facts th:contains(Duration) + td").text();
        String   ingress  = doc.select(".ingress").text();
        return Map.of(
                "name", name,
                "duration", duration,
                "ingress", ingress.substring(0, Math.min(ingress.length(), 100))
        );
    }

    void printSummary(Integer numCourses, long elapsedTotal, long elapsedPhase1) {
        System.out.println("----------------------------");
        System.out.printf("Processed  : %d course pages%n", numCourses);
        System.out.printf("Total Time : %.3f seconds%n", elapsedTotal * 1E-9);
        System.out.printf("Fetch Links: %.3f seconds%n", elapsedPhase1 * 1E-9);
        System.out.printf("Fetch Text : %.3f seconds%n", (elapsedTotal - elapsedPhase1) * 1E-9);
    }

    HttpClient client() {
        return HttpClient.newBuilder()
                .followRedirects(NORMAL)
                .connectTimeout(ofSeconds(10))
                .build();
    }

    HttpRequest GET(String url) {
        return HttpRequest.newBuilder()
                .uri(URI.create(url))
                .GET()
                .build();
    }

}
