public class PrintVersion {
    public static void  main(String[] args) {
        System.out.printf("Java version: %s%n", System.getProperty("java.version", "no version"));
    }
}
