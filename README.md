Java Advanced, 3 days
====

Welcome to this course in Java.
The syllabus can be found [here](https://www.ribomation.se/courses/java/java-advanced.html).

Here you will find
* Installation instructions
* Solutions to the programming exercises
* Source code to demo projects


Installation Instructions
====
In order to do the programming exercises of the course, you need to have

Java JDK, version 12 installed. 
* [AdoptOpenJDK Download](https://adoptopenjdk.net/?variant=openjdk12&jvmVariant=hotspot)
  - N.B. This is not the ordinary Oracle download above, because starting with version 11, 
  Oracle plan to charge for support of Java, which means you are better off going with OpenJDK
  in the first place. This predicament is something we discuss on the course.


You also need a decent Java IDE. 
* [JetBrains IntellJ IDEA](https://www.jetbrains.com/idea/download) - This is out favourite.

In addition, you need a GIT client to easily get the solutions and demo code of the course.
* [GIT Client Download](https://git-scm.com/downloads)

Using SDKMAN in GIT Bash
----

The recommended way of installing JDK tools is to use SDKMAN.
However, it only works in a BASH (or *NIX like environment). 

For running it on Windows, you need to use MinGW (or CygWin). 
When you install GIT for Windows, you should select the option 
of bundling GIT BASH and MinGW Unix-like tools during the installation. 
Using GIT BASH, will be our platform of choice during the course.

### Prepare for SDKMAN
For SDKMAN to install in MinGW, you need install a few additional programs to MinGW. 

You can find GNU Win executables at the [GNU Win Repo](https://sourceforge.net/projects/gnuwin32/files/).

1. Locate and download the [ZIP bundle](https://sourceforge.net/projects/gnuwin32/files/zip/3.0/zip-3.0-bin.zip/download).
Then unpack the content into the /usr/bin directory of the GIT MinGW installation. For example, `C:\Program Files\Git\usr\bin`

1. It's also recommended to download/install the [tree](https://sourceforge.net/projects/gnuwin32/files/tree/1.5.2.2/tree-1.5.2.2-bin.zip/download) command.

### Install SDKMAN
Then, proceed and [install SDKMAN](https://sdkman.io/install)

    curl -s "https://get.sdkman.io" | bash
    source "$HOME/.sdkman/bin/sdkman-init.sh"
    sdk version

### Install JDK
When SDKMAN is working, you can easily
[install JDK and other JVM tools](https://sdkman.io/usage)

    sdk install java 12.0.1-open

### Explore SDKMAN
If you quickly want to list all available tools, in a more compact way than
`sdk list`, use the following one-liner

    sdk list | awk 'BEGIN {RS="[-]+\n"; FS="\n"} {print $1}'

Usage of this GIT Repo
====

You need to have a GIT client installed to clone this repo. Otherwise, you can just click on the download button and grab it all as a ZIP or TAR bundle.

* [GIT Client Download](https://git-scm.com/downloads)

Get the sources initially by a `git clone` operation. We recommend to create a top-level directory for this course and two sub-directories; one for your own solutions and one for this GIT repo. 

Open a GIT BASH window and type
    
    mkdir -p ~/java-course/my-solutions
    cd ~/java-course
    git clone https://gitlab.com/ribomation-courses/java/java-advanced.git

Get the latest updates of this repo by a `git pull` operation

    cd ~/java-course/gitlab
    git pull



Links to Large Files
====
For some of the exercises, you might want to use a large input file. Here are some compressed large text files to use.
* [English Text, 100MB (_38MB compressed_)](https://docs.ribomation.se/java/java-8/english.100MB.gz)
* [English Text, 1024MB (_396MB compressed_)](https://docs.ribomation.se/java/java-8/english.1024MB.gz)
* [Climate Data, 2,5GB (_432MB compressed_)](https://docs.ribomation.se/java/java-8/climate-data.txt.gz)


***
*If you have any questions, don't hesitate to contact me*<br>
**Jens Riboe**<br/>
Ribomation AB<br/>
[jens.riboe@ribomation.se](mailto:jens.riboe@ribomation.se)<br/>
[www.ribomation.se](https://www.ribomation.se)<br/>
