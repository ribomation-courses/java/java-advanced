package ribomation.java8.composition;
import java.util.concurrent.CompletableFuture;

public abstract class BaseStuff {
    CompletableFuture<Integer> delayedValue(int value) {
        return CompletableFuture.supplyAsync(() -> {
            delay(1);
            return value;
        });
    }

    void delay(int numSecs) {
        try {
            Thread.sleep(numSecs * 1000);
        } catch (InterruptedException ignore) { }
    }
}
