package ribomation.java8.simple_async_chains;

public class DelayedValue extends BaseStuff {
    public static void main(String[] args) {
        new DelayedValue().run();
    }

    void run() {
        long start  = System.nanoTime();
        int  answer = delayedValue(42, 2).join();
        System.out.printf("The answer is %d. Elapsed %.4f secs%n",
                answer,
                (System.nanoTime() - start) * 1E-9);
    }

}
