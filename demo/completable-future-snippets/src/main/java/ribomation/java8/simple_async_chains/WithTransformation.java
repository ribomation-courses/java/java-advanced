package ribomation.java8.simple_async_chains;

public class WithTransformation extends BaseStuff {
    public static void main(String[] args) throws Exception {
        new WithTransformation().run();
    }

    void run() {
        long start = System.nanoTime();
        delayedValue(42, 2)
                .thenApply(answer -> 10 * answer)
                .thenAccept(answer ->
                        System.out.printf("The answer is %d. Elapsed %.4f secs%n",
                                answer,
                                (System.nanoTime() - start) * 1E-9)
                )
                .join();
    }

}
