package ribomation.java8.simple_async_chains;

public class WithRunnable extends BaseStuff {
    public static void main(String[] args) throws Exception {
        new WithRunnable().run();
    }

    void run() {
        long start = System.nanoTime();
        delayedValue(42, 2)
                .thenRun(() ->
                        System.out.printf("The answer is %d. Elapsed %.4f secs%n",
                                -1, (System.nanoTime() - start) * 1E-9)
                )
                .join();
    }

}
