package ribomation.java8.multiple_chains;
import java.util.concurrent.CompletableFuture;

public class UsingAnyOf extends BaseStuff {
    public static void main(String[] args) {
        new UsingAnyOf().run();
    }

    void run() {
        CompletableFuture<Integer> chain1 = delayedValue(10, 4);
        CompletableFuture<Integer> chain2 = delayedValue(4, 3);
        CompletableFuture<Integer> chain3 = delayedValue(7, 1);
        CompletableFuture<Integer> chain4 = delayedValue(21, 2);

        long start = System.nanoTime();
        Integer answer = (Integer) CompletableFuture.anyOf(chain1, chain2, chain3, chain4).join();
        System.out.printf("The answer is %d. Elapsed %.4f secs%n",
                answer,
                (System.nanoTime() - start) * 1E-9);
    }

}
