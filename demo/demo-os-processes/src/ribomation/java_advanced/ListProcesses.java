package ribomation.java_advanced;

import java.util.stream.Collectors;

public class ListProcesses {
    public static void main(String[] args) {
        ProcessHandle.allProcesses()
                .filter(p -> p.info().user().orElse("").contains("jens"))
                .map(p -> p.info().command().orElse(""))
                .collect(Collectors.toSet())
                .forEach(System.out::println);
    }
}


