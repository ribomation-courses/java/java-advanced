package ribomation.java_advanced.philosofers;

public class NoDeadlockApp extends App {
    public static void main(String[] args) {
        new NoDeadlockApp().run(args);
    }

    protected Philosopher createPhilosopher(int k) {
        final int N     = chopSticks.size();
        ChopStick left  = chopSticks.get(k % N);
        ChopStick right = chopSticks.get((k + 1) % N);
        if (k == 0) {
            ChopStick tmp = left;
            left = right;
            right = tmp;
        }
        return new Philosopher(k, left, right);
    }
}
