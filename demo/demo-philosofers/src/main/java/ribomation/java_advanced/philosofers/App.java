package ribomation.java_advanced.philosofers;

import java.util.ArrayList;
import java.util.List;

public abstract class App {
    private int               numPhilosophers = 5;
    protected List<ChopStick>   chopSticks      = new ArrayList<>();
    protected List<Philosopher> philosophers    = new ArrayList<>();

    protected abstract Philosopher createPhilosopher(int k);

    protected void run(String[] args) {
        try {
            parseArgs(args);
            setup();
            launch();
        } catch (InterruptedException ignore) {
        }
    }

    private void parseArgs(String[] args) {
        for (int k = 0; k < args.length; k++) {
            if (args[k].equals("-p")) {
                numPhilosophers = Integer.parseInt(args[++k]);
            }
        }
        System.out.printf("[app] philosophers = %d%n", numPhilosophers);
    }

    private void setup() throws InterruptedException {
        for (int k = 0; k < numPhilosophers; ++k) {
            chopSticks.add(new ChopStick());
        }
        for (int k = 0; k < chopSticks.size(); ++k) {
            philosophers.add(createPhilosopher(k));
        }

    }

    private void launch() throws InterruptedException {
        for (Philosopher p : philosophers) p.start();
        for (Philosopher p : philosophers) p.join();
    }
}
