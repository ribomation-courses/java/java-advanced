package ribomation.java_intermediate.security.policies;

public class ReadSysProps {
    public static void main(String[] args) {
        ReadSysProps app = new ReadSysProps();
        app.sysprop("os.name");
        app.sysprop("java.version");
        app.sysprop("user.home");
        app.sysprop("java.home");
    }

    void sysprop(String name) {
        try {
            System.out.printf("Try '%s'%n", name);
            System.out.printf("sysprop[%s] = '%s'%n", name, System.getProperty(name, "*** not found"));
        } catch (Exception e) {
            System.out.printf("error: %s%n", e);
        }
    }
}
