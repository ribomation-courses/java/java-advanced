package ribomation.java_intermediate.security.policies;

public class ReadSysPropsWithStdSecMgr {
    public static void main(String[] args) {
        System.setProperty("java.security.policy", "sysprops.policy");
        System.setSecurityManager(new SecurityManager());

        ReadSysPropsWithStdSecMgr app = new ReadSysPropsWithStdSecMgr();
        app.sysprop("os.name");
        app.sysprop("java.version");
        app.sysprop("user.home");
        app.sysprop("java.home");
    }

    void sysprop(String name) {
        try {
            System.out.printf("Try '%s'%n", name);
            System.out.printf("sysprop[%s] = '%s'%n", name, System.getProperty(name, "*** not found"));
        } catch (Exception e) {
            System.out.printf("error: %s%n", e);
        }
    }
}
