package ribomation.java_intermediate.security.policies;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.stream.Collectors;

public class ReadDummyFile {
    public static void main(String[] args) throws FileNotFoundException {
        System.setSecurityManager(new SecurityManager());
        String path = "./settings.gradle";
        ReadDummyFile app = new ReadDummyFile();
        app.read(path);
    }

     void read(String path) throws FileNotFoundException {
         FileReader fr = new FileReader(path);
         BufferedReader in = new BufferedReader(fr);
         String content = in.lines().collect(Collectors.joining("\n"));
         System.out.printf("[%s] %s%n", path, content);
     }
}
