package ribomation.java_advanced.swift;

import java.util.Locale;

public class ReceivingBank extends Thread {
    private final MoneyBox moneyBox;
    private long sum   = 0;
    private long count = 0;

    public ReceivingBank(MoneyBox moneyBox) {
        super("receiver");
        this.moneyBox = moneyBox;
    }

    @Override
    public void run() {
        int amount = 0;
        while ((amount = moneyBox.recv()) != MoneyBox.STOP) {
            sum += amount;
            count++;
        }
    }

    public long getSum() {
        return sum;
    }

    public long getCount() {
        return count;
    }

    @Override
    public String toString() {
        return String.format(Locale.UK, "[%s] sum = %,d, count*AMOUNT = %,d",
                getName(), getSum(), getCount() * MoneyBox.AMOUNT);
    }
}
