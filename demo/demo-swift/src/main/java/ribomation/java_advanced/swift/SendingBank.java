package ribomation.java_advanced.swift;

import java.util.Locale;

public class SendingBank extends Thread {
    private final MoneyBox moneyBox;
    private final int      N;

    public SendingBank(MoneyBox moneyBox, int n) {
        super("sender");
        this.moneyBox = moneyBox;
        N = n;
    }

    @Override
    public void run() {
        System.out.printf("[%s] started%n", getName());
        for (int k = 0; k < N; ++k) moneyBox.send(MoneyBox.AMOUNT);
        moneyBox.send(MoneyBox.STOP);
        System.out.printf("[%s] done%n", getName());
    }

    @Override
    public String toString() {
        return String.format(Locale.UK, "[%s] sent amount = %,d",
                getName(), N * MoneyBox.AMOUNT);
    }
}
