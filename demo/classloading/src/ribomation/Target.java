package ribomation;

import java.util.Random;

public class Target implements Runnable {
    private static int staticNumber = 42;
    private final String name;

    static {
        System.out.printf("*** Loaded class %s%n", Target.class.getName());
    }

    public Target(String name) {
        this.name = name;
    }

    public static void shake() {
        staticNumber = new Random(System.nanoTime() % 97).nextInt(100);
    }

    @Override
    public String toString() {
        return String.format("%s{%s, number=%d, id=%d}",
                getClass().getSimpleName(),
                name,
                staticNumber,
                getClass().hashCode()
        );
    }

    @Override
    public void run() {
        System.out.printf("BEFORE %s%n", this);
        shake();
        System.out.printf("AFTER %s%n", this);
    }
}
