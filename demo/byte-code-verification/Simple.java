public class Simple {
  public static void main(String[] args) {
    new Simple().run();
  }
  void run() {
    System.out.println( greeting() );
    System.out.println( compute() );
  }
  String greeting() {
    return "Hi Java";
  }
  int compute() {
    int a=40, b=2;
    return a + b;
  }
}
