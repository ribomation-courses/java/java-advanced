package ribomation.java8.extracting_links;
import org.jsoup.nodes.Element;
import java.net.URL;
import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.stream.Collectors;

public class CourseIngressTexts extends BaseStuff {
    public static void main(String[] args) throws Exception {
        new CourseIngressTexts().run();
    }

    void run() {
        long start = System.nanoTime();
        extractLinks(mkUrl(course))
                .thenApply(links -> links.stream()
                        .map(this::extractIngress))
                .thenApplyAsync(promises -> promises.map(CompletableFuture::join))
                .thenApply(txtStream -> txtStream
                        .map(str -> str.substring(0, Math.min(100, str.length()))))
                .thenAccept(txtStream -> {
                    txtStream.forEach(System.out::println);
                    System.out.printf("Elapsed %.3f seconds%n",
                            (System.nanoTime() - start) * 1E-9);
                })
                .join();
    }

    CompletableFuture<List<URL>> extractLinks(URL url) {
        return fetchDoc(url)
                .thenApply(doc -> doc.select(".schedule a"))
                .thenApply(nodes -> nodes.stream()
                        .map(node -> node.attr("href"))
                        .map(this::mkUrl)
                        .collect(Collectors.toList()))
                ;
    }

    CompletableFuture<String> extractIngress(URL url) {
        return fetchDoc(url)
                .thenApply(doc -> doc.select(".course-ingress p"))
                .thenApply(nodes -> nodes.stream()
                        .limit(1)
                        .map(Element::text)
                        .map(txt -> String.format("%s: %s",
                                url.getFile().replace(".html", ""), txt))
                        .findAny()
                        .orElse(""))
                ;
    }

}
