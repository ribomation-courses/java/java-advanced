package ribomation.java8.simple_fetch;
import org.asynchttpclient.AsyncHttpClient;
import org.asynchttpclient.DefaultAsyncHttpClient;
import org.asynchttpclient.ListenableFuture;
import org.asynchttpclient.Response;
import java.io.IOException;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;

public class SyncFetch extends BaseStuff {
    public static void main(String[] args) throws Exception {
        new SyncFetch().run();
    }

    void run() throws IOException, ExecutionException, InterruptedException {
        try (AsyncHttpClient http = new DefaultAsyncHttpClient()) {
            Future<Response> future = http.prepareGet(url).execute();
            //do other stuff...
            Response response = future.get(); //wait here for result
            print(response);
        }
    }

}
